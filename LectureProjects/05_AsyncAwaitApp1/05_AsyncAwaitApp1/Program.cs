﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace _05_AsyncAwaitApp1 {
    class Program {

        // First async method in the world
        static async Task<int> GetNumberFromLongRunningProcessAsync() {
            int res = await Task.Run(() => {
                Console.WriteLine("Starting long running operation");
                Thread.Sleep(10000);
                Console.WriteLine("Long running operation done");
                return 5;
            });
            return res;
        }

        // First async method in the world
        static async Task<int> GetNumberFromLongRunningProcessAsync_old() {
            Task<int> t = new Task<int>(() => {
                Console.WriteLine("Starting long running operation");
                Thread.Sleep(10000);
                Console.WriteLine("Long running operation done");
                return 5;
            });
            t.Start();
            int res = await t;
            return res;
        }

        // Using the async GetNumberFromLongRunningProcessAsync is simple
        // if you continue using the async methods. Just dont block anywhere
        // outside await
        static async Task<int> UseNumberAsync() {
            int result = await GetNumberFromLongRunningProcessAsync();

            result++;

            return result * result;
        }

        // Same as UseNumberAsync but we need to handle the Wait()
        // This can give code that is hard to read. It is also your
        // responsibility to Wait for long enough.
        static int UseNumber() {
            Task<int> t = GetNumberFromLongRunningProcessAsync();
            if (!t.Wait(20000)) // 10sec ? 20sec ? ...
                throw new Exception("The long running process did not finish");

            int result = t.Result;

            result++;

            return result * result;
        }

        // Getting out of async/await
        public static void Demo1() {
            Task<int> t = UseNumberAsync();
            if (!t.Wait(20000)) // 10sec ? 20sec ? ...
                throw new Exception("The long running process did not finish");

            Console.WriteLine("Calculation gave : " + t.Result);
        }

        // Getting out of async/await
        public static void Demo2() {
            int res = UseNumber();
            
            Console.WriteLine("Calculation gave : " + res);
        }
        
        // You can find other examples online
        // One example at https://www.dotnetperls.com/async
        // Have fun
        

        static void Main(string[] args) {

            Demo1();
            Demo2();

            Console.WriteLine("Hit a key to return from Main");
            Console.ReadKey();
        }
    }
}
