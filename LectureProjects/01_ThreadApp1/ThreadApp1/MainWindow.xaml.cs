﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ThreadApp1 {

    public class MyThreadClass {

        private readonly object section = new object();
        private Thread thread;
        private Queue<string> msg = new Queue<string>();        

        public MyThreadClass() {            
        }

        public void Run() {
            thread = new Thread(new ThreadStart(ThreadLoop));
            thread.Start();

            //thread = new Thread(new ParameterizedThreadStart(ThreadLoopWithArg));
            //thread.Start("Hello world from ThreadLoopWithArg");
        }

        public void Stop() {
            // Awful but I'm in a hurry :(
            thread.Abort();
            thread = null;

            // Better: Shutdown variable
        }

        private void ThreadLoopWithArg(object arg) {
            Trace.WriteLine(arg);
            ThreadLoop();
        }

        public string GetMessage() {
            lock (section) {
                if (msg.Count > 0)
                    return msg.Dequeue();
            }
            return null;
        }

        private void ThreadLoop() {
            int counter = 0;
            try {
                while (true) {
                    ++counter;

                    lock (section) {
                        msg.Enqueue("counter = " + counter);        
                    }
                    
                    Thread.Sleep(2000);
                }
            } catch (Exception e) {
                Trace.WriteLine("ThreadLoop EXCEPTION : " + e.Message);
            } finally {
                Trace.WriteLine("ThreadLoop exited");
                MessageBox.Show("Thread Ended");
            }
        }
    }

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {

        private MyThreadClass myThreadClass = null;

        public MainWindow() {
            InitializeComponent();
        }

        public void Log(string s) {
            textBox.AppendText(DateTime.Now + " : " + s + "\n");
        }

        private void Stop_Click(object sender, RoutedEventArgs e) {
            Log("Stop_Click");
            if (myThreadClass is null)
                return;
            myThreadClass.Stop();
            myThreadClass = null;
        }

        private void Run_Click(object sender, RoutedEventArgs e) {
            Log("Run_Click");
            if (myThreadClass != null) {
                myThreadClass.Stop();
                myThreadClass = null;
            }
            myThreadClass = new MyThreadClass();
            myThreadClass.Run();            
        }

        private void Wait_Click(object sender, RoutedEventArgs e) {
            Thread.Sleep(20000);
        }

        private void GetMsg_Click(object sender, RoutedEventArgs e) {
            if (myThreadClass is null) {
                Log("No thread running");
                return;
            }
            string msg;

            while ((msg = myThreadClass.GetMessage()) != null)
                Log(msg);
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e) {
            if (myThreadClass is null)
                return;
            myThreadClass.Stop();
            myThreadClass = null;
        }
    }
}
