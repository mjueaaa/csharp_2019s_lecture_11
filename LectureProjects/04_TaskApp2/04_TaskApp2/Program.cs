﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace _04_TaskApp2 {
    class Program {

        public static void Demo1_Simple_Task() {
            Action<object> action = (object obj) => {
                Console.WriteLine("Task={0}, obj={1}, Thread={2}", Task.CurrentId, obj, Thread.CurrentThread.ManagedThreadId);

                Thread.Sleep(10000);
            };

            Task t1 = new Task(action, "alpha");
            t1.Start();

            Console.WriteLine("Calling wait on task");

            t1.Wait();
            Console.WriteLine("Wait returned");
        }

        public static void Demo2_Many_Tasks() {
            Action<object> action = (object obj) => {
                Console.WriteLine("Task={0}, obj={1}, Thread={2}", Task.CurrentId, obj, Thread.CurrentThread.ManagedThreadId);
                Thread.Sleep(20000);
            };

            List<Task> tasks = new List<Task>();
            for (int i = 0; i < 20; ++i) {
                Task t = new Task(action, "Task "+i);
                t.Start();
                tasks.Add(t);
            }

            Console.WriteLine("Calling wait on all tasks");

            for (int i = 0; i < 20; ++i) {
                tasks[i].Wait();
                Console.WriteLine($"Wait {i} returned");
            }
        }

        public static void MyFirstMethod(object obj) {
            Console.WriteLine("Running Task={0}, obj={1}, Thread={2}", Task.CurrentId, obj, Thread.CurrentThread.ManagedThreadId);
            Thread.Sleep(4000);
        }
        public static void MySecondMethod(object obj) {
            Console.WriteLine("Finihing Task={0}, obj={1}, Thread={2}", Task.CurrentId, obj, Thread.CurrentThread.ManagedThreadId);
            Thread.Sleep(10000);
            Console.WriteLine("Finihing done");
        }

        public static void Demo3_ContinueWith() {            

            Task t1 = new Task(new Action<object>(MyFirstMethod), "alpha");

            t1.ContinueWith(MySecondMethod);

            t1.Start();

            Console.WriteLine("Calling wait on task");

            t1.Wait();
            Console.WriteLine("Wait returned");
        }

        public static int MyReturningMethod(object obj) {
            Console.WriteLine("Running Task={0}, obj={1}, Thread={2}", Task.CurrentId, obj, Thread.CurrentThread.ManagedThreadId);
            Thread.Sleep(1000);
            return 100;            
        }

        public static void Demo4_Generics() {

            Task<int> t1 = new Task<int>(MyReturningMethod,"");

            t1.Start();

            Console.WriteLine("Calling wait on task");

            t1.Wait();
            Console.WriteLine("Wait returned. Result : " + t1.Result);
        }

        static void Main(string[] args) {

            //Demo1_Simple_Task();
            //Demo2_Many_Tasks();
            //Demo3_ContinueWith();
            Demo4_Generics();

            Console.ReadKey();
        }
    }
}
