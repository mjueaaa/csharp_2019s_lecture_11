﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TaskApp1 {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {
        Task t1 = null;

        public MainWindow() {
            System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.InvariantCulture;
            System.Threading.Thread.CurrentThread.CurrentUICulture = System.Globalization.CultureInfo.InvariantCulture;

            InitializeComponent();
        }

        public void Log(string s) {
            // textBox.AppendText(DateTime.Now + " : " + s + "\n"); // WARNING
            Trace.WriteLine(DateTime.Now + " : " + s + "");
        }

        private void Run_Click(object sender, RoutedEventArgs e) {

            Action<object> action = (object obj) => {
                Log($"Task={Task.CurrentId}, obj={obj}, Thread={Thread.CurrentThread.ManagedThreadId}");
            };

            // Create a task but do not start it.
            t1 = new Task(action, "alpha");
            t1.Start();
            textBox.AppendText("Started task\n");
        }

        private void Wait_Click(object sender, RoutedEventArgs e) {
            if (t1 != null) {
                t1.Wait();
                textBox.AppendText("Task.Wait returned\n");
            }
        }

    }
}
